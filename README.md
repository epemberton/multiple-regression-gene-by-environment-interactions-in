# README #

### Project Description ###

Discovered and evaluated a model to account for an individual’s depression experience given certain gene,
environment, and gene-environment variables using multiple regression – specifically forward step-wise
regression and LASSO regression.